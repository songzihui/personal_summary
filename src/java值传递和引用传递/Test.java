package java值传递和引用传递;

class Emp {
    public int age;
}
public class Test {
    public static void change(Emp emp)
    {
        emp.age = 50;
        emp = new Emp();
        emp.age=100;
        System.out.println("方法内"+emp.age);
    }

    public static void main(String[] args) {
        Emp emp = new Emp();
        emp.age = 100;
        System.out.println(emp.age);

        change(emp);
        System.out.println(emp.age);
        System.out.println(emp.age);
    }
}