package java值传递和引用传递;

public class demo {

    /*
    *首先要说明的是java中是没有指针的，java中只存在值传递，只存在值传递！！！
      *  然而我们经常看到对于对象（数组，类，接口）的传递似乎有点像引用传递，可以改变对象中某个属性的值。
      *  但是不要被这个假象所蒙蔽，实际上这个传入函数的值是对象引用的拷贝，即传递的是引用的地址值，所以还是按值传递。
    * */

    public static void change(int a){
        a=50;
    }
    public static void main(String[] args) {
        int a=10;
        System.out.println(a);
        change(a);
        System.out.println(a);
    }
    //基本数据类型传值不变，在栈中
}
