SELECT * from employee e,person p WHERE e.Id=p.id;

左连接
---显示所有的左边数据，右边能关联上的显示，不能关联上的为空
SELECT * FROM employee e LEFT JOIN person p ON e.Id=p.Id;

右连接
---显示所有的右边数据，左边能关联上的显示，不能关联上的为空
SELECT * FROM employee e RIGHT JOIN person p ON e.Id=p.Id

内连接
---显示能关联上的数据，没有关联上的不显示
SELECT * FROM employee e INNER JOIN person p ON e.Id=p.Id

UNION
--UNION 操作符用于合并两个或多个 SELECT 语句的结果集。UNION 内部的 SELECT 语句必须拥有相同数量的列。列也必须拥有相似的数据类型。同时，每条 SELECT 语句中的列的顺序必须相同。
SELECT E_Name,e_id FROM Employees_China
UNION
SELECT E_Name,e_id FROM Employees_USA

UNION ALL 筛选的值允许重复

UNION 结果集中的列名总是等于 UNION 中第一个 SELECT 语句中的列名

SQL TOP 子句
--TOP 子句用于规定要返回的记录的数目。对于拥有数千条记录的大型表来说，TOP 子句是非常有用的。注释：并非所有的数据库系统都支持 TOP 子句。
   MySQL 语法
    SELECT *
    FROM Persons
    LIMIT 5
  Oracle 语法
    SELECT *
    FROM Persons
    WHERE ROWNUM <= 5

    SELECT TOP 2 * FROM Persons

SQL LIKE 操作符
--LIKE 操作符用于在 WHERE 子句中搜索列中的指定模式。

      SELECT * FROM Persons
    WHERE City LIKE 'N%' --以 "N" 开始的城市里的人

    SELECT * FROM Persons
    WHERE City LIKE '%g' --以 "g" 结尾的城市里的人

    SELECT * FROM Persons
    WHERE City LIKE '%lon%' --包含 "lon" 的城市里的人

    SELECT * FROM Persons
    WHERE City NOT LIKE '%lon%'  --不包含 "lon" 的城市里的人

SQL IN 操作符

--  IN 操作符 IN 操作符允许我们在 WHERE 子句中规定多个值。
  SELECT * FROM Persons
  WHERE LastName IN ('Adams','Carter')

--BETWEEN 操作符在 WHERE 子句中使用，作用是选取介于两个值之间的数据范围。
  SELECT * FROM Persons
WHERE LastName
BETWEEN 'Adams' AND 'Carter'


SQL GROUP BY 语句
  --GROUP BY 语句用于结合合计函数，根据一个或多个列对结果集进行分组。
    SELECT Customer,SUM(OrderPrice) FROM Orders --显示的列是顾客，每个顾客下order的所有和总和
  GROUP BY Customer  --以order by 的字段进行sum  如果不适用order by 语句指定了两列Customer 和 SUM(OrderPrice)。"SUM(OrderPrice)" 返回一个单独的值"OrderPrice" 列的总计 "Customer" 返回 6 个值每个值对应 "Orders" 表中的每一行
--可以用1,2表示order后面排序的列
SQL HAVING 子句
 -- 在 SQL 中增加 HAVING 子句原因是，WHERE 关键字无法与合计函数一起使用。
    SELECT Customer,SUM(OrderPrice) FROM Orders
  GROUP BY Customer
  HAVING SUM(OrderPrice)<2000  ---having对统计的字做限制




SELECT INTO 语句
-- SELECT INTO 语句从一个表中选取数据，然后把数据插入另一个表中。MySQL不支持Select Into语句直接备份表结构和数据,一些种方法可以代替。
Create table Employees_USA2 (Select * from Employees_USA);
--IN 子句可用于向另一个数据库中拷贝表
SELECT *
INTO Persons IN 'Backup.mdb'
FROM Persons
--如果我们希望拷贝某些域，可以在 SELECT 语句后列出这些域：
SELECT LastName,FirstName
INTO Persons_backup
FROM Persons
WHERE City='Beijing'

SELECT Persons.LastName,Orders.OrderNo
INTO Persons_Order_Backup
FROM Persons
INNER JOIN Orders
ON Persons.Id_P=Orders.Id_P


CREATE DATABASE 语句
--CREATE DATABASE 用于创建数据库。
    CREATE DATABASE database_name

CREATE TABLE 语句

    CREATE TABLE 表名称
    (
    列名称1 数据类型,
    Id_P int NOT NULL UNIQUE,
    列名称2 数据类型,
    LastName varchar(255) NOT NULL,
    列名称3 数据类型,
    UNIQUE (Id_P)
    PRIMARY KEY (Id_P)
    FOREIGN KEY (Id_P) REFERENCES Persons(Id_P)
    CHECK (Id_P>0)
    ....
    )

SQL 约束 (Constraints)
--约束用于限制加入表的数据的类型。可以在创建表时规定约束通过 CREATE TABLE 语句，或者在表创建之后也可以通过 ALTER TABLE 语句
    NOT NULL -- NOT NULL 约束强制列不接受 NULL 值。NOT NULL 约束强制字段始终包含值。这意味着，如果不向字段添加值，就无法插入新记录或者更新记录。
    UNIQUE --UNIQUE 约束唯一标识数据库表中的每条记录。 UNIQUE 和 PRIMARY KEY 约束均为列或列集合提供了唯一性的保证。PRIMARY KEY 拥有自动定义的 UNIQUE 约束。请注意，每个表可以有多个 UNIQUE 约束，但是每个表只能有一个 PRIMARY KEY 约束。
    PRIMARY KEY--PRIMARY KEY 约束唯一标识数据库表中的每条记录。主键必须包含唯一的值。主键列不能包含 NULL 值。每个表都应该有一个主键，并且每个表只能有一个主键。
    FOREIGN KEY --一个表中的 FOREIGN KEY 指向另一个表中的 PRIMARY KEY。FOREIGN KEY 约束用于预防破坏表之间连接的动作。FOREIGN KEY 约束也能防止非法数据插入外键列，因为它必须是它指向的那个表中的值之一。FOREIGN KEY 约束也能防止非法数据插入外键列，因为它必须是它指向的那个表中的值之一。
    CHECK --CHECK 约束用于限制列中的值的范围。如果对单个列定义 CHECK 约束，那么该列只允许特定的值。如果对一个表定义 CHECK 约束，那么此约束会在特定的列中对值进行限制。
    DEFAULT --DEFAULT 约束用于向列中插入默认值。如果没有规定其他的值，那么会将默认值添加到所有的新记录。


Unique Key 与 Primary Key 的比较

不同点:
--1. 一张表只能有唯一的一个 Primary Key, 但可以拥有多个 Unique Key
--2. Primary Key 不允许空值，但 Unique Key允许空值
--3. Primary Key自动在列上建立聚簇索引, 而 unique Key不建立索引!

相同点:
--两者都保证所在列的记录的唯一性!
--REFERENCES 参考( reference的名词复数 ); 证明书; 为方便查询所用的 标记; 帮助或意见的 征求;
--CONSTRAINT 约束; 限制; 强制

添加外键
ALTER TABLE Orders
ADD CONSTRAINT fk_PerOrders--添加外键
FOREIGN KEY (Id_P)--外键名字
REFERENCES Persons(Id_P) --外键指向哪个表的主键关联

--如果在 "Orders" 表已存在的情况下为 "Id_P" 列创建 FOREIGN KEY 约束，请使用下面的 SQL：
ALTER TABLE Orders
ADD FOREIGN KEY (Id_P)
REFERENCES Persons(Id_P)

添加主键
ALTER TABLE Persons
ADD CONSTRAINT pk_PersonID PRIMARY KEY (Id_P,LastName)

添加约束
CONSTRAINT chk_Person CHECK (Id_P>0 AND City='Sandnes')


SQL CREATE INDEX 语句

--CREATE INDEX 语句用于在表中创建索引。在不读取整个表的情况下，索引使数据库应用程序可以更快地查找数据。

--您可以在表中创建索引，以便更加快速高效地查询数据。用户无法看到索引，它们只能被用来加速搜索/查询。
--更新一个包含索引的表需要比更新一个没有索引的表更多的时间，这是由于索引本身也需要更新。因此，理想的做法是仅仅在常常被搜索的列以及表上面创建索引。

CREATE UNIQUE INDEX index_name --带unique是否是唯一索引
ON table_name (column_name)

SQL 撤销索引 表以及数据库
--通过使用 DROP 语句，可以轻松地删除索引、表和数据库。
sqlserver
DROP INDEX index_name ON table_name
mysql
DALTER TABLE table_name DROP INDEX index_name
oracle
DROP INDEX index_name

ALTER TABLE 语句用于在已有的表中添加 修改或删除列
添加列
ALTER TABLE table_name
ADD column_name datatype
删除列
ALTER TABLE table_name
DROP COLUMN column_name

SQL AUTO INCREMENT 字段
---Auto-increment 会在新记录插入表中时生成一个唯一的数字。我们通常希望在每次插入新记录时，自动地创建主键字段的值。我们可以在表中创建一个 auto-increment 字段。
CREATE TABLE Persons
(
P_Id int NOT NULL AUTO_INCREMENT,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255),
PRIMARY KEY (P_Id)
)

SQL VIEW 视图
--在 SQL 中，视图是基于 SQL 语句的结果集的可视化的表。
--视图包含行和列，就像一个真实的表。视图中的字段就是来自一个或多个数据库中的真实的表中的字段。我们可以向视图添加 SQL 函数、WHERE 以及 JOIN 语句，我们也可以提交数据，就像这些来自于某个单一的表。

SQL CREATE VIEW 语法

创建视图
    CREATE VIEW view_name AS
    SELECT column_name(s)
    FROM table_name
    WHERE condition
查询视图
    SELECT * FROM [Current Product List]

乐观锁和悲观锁
  悲观锁-----多写场景
  总是假设最坏的情况,每次拿数据的时候总认为别人会修改,每次在拿数据的时候都会上锁,这样别人想拿这个数据就会阻塞直到它拿到锁
  (共享资源每次只给一个线程使用,其它线程阻塞,用完后再把资源转让给其它线程)传统的关系型数据库里边就用到了很多这种锁机制,
  比如行锁,表锁等,读锁,写锁等,都是在做操作之前先上锁,Java中synchronized和ReentrantLock等独占锁就是悲观锁思想的实现
  乐观锁-----多读场景
  总是假设最好的情况,每次去拿数据的时候都认为别人不会修改,所以不会上锁,但是在更新的时候会判断一下在此期间别人有没有去更新这个数据
  可以使用版本号机制和CAS算法实现,乐观锁适用于多读的应用类型,这样可以提高吞吐量,像数据库提供的类似于write_condition机制
  其实都是提供的乐观锁.在Java中java.util.concurrent.atomic包下面的原子变量类就是使用了乐观锁的一种实现方式CAS实现的
乐观锁缺点
  1.ABA问题
  如果一个变量V初次读取的时候是A值,并且在准备赋值的时候检查到它仍然是A值,那我们就能说明它的值没有被其他线程修改过了吗
  很明显是不能的,因为在这段时间它的值可能被改为其他值,然后又改回A,那CAS操作就会误认为它从来没有被修改过
  这个问题被称为CAS操作的ABA问题,AtomicStampedReference 类提供此种能力compareAndSet 方法
  2.循环时间长开销大
  自旋CAS也就是不成功就一直循环执行直到成功如果长时间不成功,会给CPU带来非常大的执行开销
  3.只能保证一个共享变量的原子操作
  CAS 只对单个共享变量有效,当操作涉及跨多个共享变量时 CAS 无效.但是从 JDK 1.5开始,提供了AtomicReference类来保证引用对象之间的原子性,你可以把多个变量放在一个对象里来进行 CAS 操作.所以我们可以使用锁或者利用AtomicReference类把多个共享变量合并成一个共享变量来操作
CAS与synchronized的使用情景
  简单的来说CAS适用于写比较少的情况下多读场景,冲突一般较少,synchronized适用于写比较多的情况下多写场景,冲突一般较多
  对于资源竞争较少线程冲突较轻的情况,使用synchronized同步锁进行线程阻塞和唤醒切换以及用户态内核态间的切换操作额外浪费消耗cpu资源;而CAS基于硬件实现,不需要进入内核,不需要切换线程,操作自旋几率较少,因此可以获得更高的性能.
  对于资源竞争严重线程冲突严重的情况,CAS自旋的概率会比较大,从而浪费更多的CPU资源,效率低于synchronized,


带有 not in 的查询结构体

SELECT
	sp.id,
	pf.real_name sp_name,
	pf.id_card_no sp_id_card_no,
	rr.creator_id sp_userId
FROM
	so_room_reservation rr
LEFT JOIN so_user cu ON cu.id = rr.creator_id
LEFT JOIN so_suspect_person sp ON sp.id = rr.id
LEFT JOIN so_personnel_file pf ON pf.id = sp.personnel_file_id
WHERE
	rr.approve_status = 2
AND rr.process_status IN (1, 2)
AND pf.deleted = 0
AND sp.id NOT IN (SELECT h.suspect_person_id FROM so_health h where h.type = 1)



查询语句中select from where group by having order by的执行顺序

select substr('Hello World',3) 子串 from dual;

select lower('Hello WOrld') 转小写
lower upper('Hello WOrld') 转大写,
lower initcap('hello world') 首字母大写

nvl2(a,b,c) 当a=null的时候,返回c;否则返回b

nullif(a,b) 当a=b的时候.返回null;否则返回a
SELECT nullif(1,2) from dual;

