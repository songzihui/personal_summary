SQL> /*
SQL> SQL的类型
SQL> 1. DML（data manipulation language 数据操作语言）:insert update delete select
SQL> 2. DDL(data definition language 数据定义语言): create table,alter table,drop table,truncate table
SQL>                                                create/drop view,sequence,index,synonym(同义词)
SQL> 3. DCL(data control language 数据控制语言）：grant(授权) revoke（撤销权限）
SQL> */
SQL> --插入insert
SQL> insert into emp(empno,ename,sal,deptno) values(1001,'Tom',2000,10);

已创建 1 行。

SQL> --PreparedStatement pst = "insert into emp(empno,ename,sal,deptno) values(?,?,?,?)"
SQL> --地址符
SQL> insert into emp(empno,ename,sal,deptno) values(&empno,&ename,&sal,&deptno);
输入 empno 的值:  1002
输入 ename 的值:  'Mary'
输入 sal 的值:  2000
输入 deptno 的值:  10
原值    1: insert into emp(empno,ename,sal,deptno) values(&empno,&ename,&sal,&deptno)
新值    1: insert into emp(empno,ename,sal,deptno) values(1002,'Mary',2000,10)

已创建 1 行。

SQL> /
输入 empno 的值:  1003
输入 ename 的值:  'Mike'
输入 sal 的值:  3000
输入 deptno 的值:  30
原值    1: insert into emp(empno,ename,sal,deptno) values(&empno,&ename,&sal,&deptno)
新值    1: insert into emp(empno,ename,sal,deptno) values(1003,'Mike',3000,30)

已创建 1 行。

SQL> select empno,ename,&t from emp;
输入 t 的值:  sal
原值    1: select empno,ename,&t from emp
新值    1: select empno,ename,sal from emp

     EMPNO ENAME             SAL                                                                                                                                                                        
---------- ---------- ----------                                                                                                                                                                        
      1001 Tom              2000                                                                                                                                                                        
      1002 Mary             2000                                                                                                                                                                        
      1003 Mike             3000                                                                                                                                                                        
      7369 SMITH             800                                                                                                                                                                        
      7499 ALLEN            1600                                                                                                                                                                        
      7521 WARD             1250                                                                                                                                                                        
      7566 JONES            2975                                                                                                                                                                        
      7654 MARTIN           1250                                                                                                                                                                        
      7698 BLAKE            2850                                                                                                                                                                        
      7782 CLARK            2450                                                                                                                                                                        
      7788 SCOTT            3000                                                                                                                                                                        

     EMPNO ENAME             SAL                                                                                                                                                                        
---------- ---------- ----------                                                                                                                                                                        
      7839 KING             5000                                                                                                                                                                        
      7844 TURNER           1500                                                                                                                                                                        
      7876 ADAMS            1100                                                                                                                                                                        
      7900 JAMES             950                                                                                                                                                                        
      7902 FORD             3000                                                                                                                                                                        
      7934 MILLER           1300                                                                                                                                                                        

已选择 17 行。

SQL> /
输入 t 的值:  job
原值    1: select empno,ename,&t from emp
新值    1: select empno,ename,job from emp

     EMPNO ENAME      JOB                                                                                                                                                                               
---------- ---------- ---------                                                                                                                                                                         
      1001 Tom                                                                                                                                                                                          
      1002 Mary                                                                                                                                                                                         
      1003 Mike                                                                                                                                                                                         
      7369 SMITH      CLERK                                                                                                                                                                             
      7499 ALLEN      SALESMAN                                                                                                                                                                          
      7521 WARD       SALESMAN                                                                                                                                                                          
      7566 JONES      MANAGER                                                                                                                                                                           
      7654 MARTIN     SALESMAN                                                                                                                                                                          
      7698 BLAKE      MANAGER                                                                                                                                                                           
      7782 CLARK      MANAGER                                                                                                                                                                           
      7788 SCOTT      ANALYST                                                                                                                                                                           

     EMPNO ENAME      JOB                                                                                                                                                                               
---------- ---------- ---------                                                                                                                                                                         
      7839 KING       PRESIDENT                                                                                                                                                                         
      7844 TURNER     SALESMAN                                                                                                                                                                          
      7876 ADAMS      CLERK                                                                                                                                                                             
      7900 JAMES      CLERK                                                                                                                                                                             
      7902 FORD       ANALYST                                                                                                                                                                           
      7934 MILLER     CLERK                                                                                                                                                                             

已选择 17 行。

SQL> select * from &t;
输入 t 的值:  dept
原值    1: select * from &t
新值    1: select * from dept

    DEPTNO DNAME          LOC                                                                                                                                                                           
---------- -------------- -------------                                                                                                                                                                 
        10 ACCOUNTING     NEW YORK                                                                                                                                                                      
        20 RESEARCH       DALLAS                                                                                                                                                                        
        30 SALES          CHICAGO                                                                                                                                                                       
        40 OPERATIONS     BOSTON                                                                                                                                                                        

SQL> rollback;

回退已完成。

SQL> host cls
create table emp10 as select * from so_user where 1=1;带数据插入
SQL> --一次插入多条数据
SQL> create table emp10 as select * from emp where 1=2;空表

表已创建。

SQL> desc emp10
 名称                                                                                                              是否为空? 类型
 ----------------------------------------------------------------------------------------------------------------- -------- ----------------------------------------------------------------------------
 EMPNO                                                                                                                      NUMBER(4)
 ENAME                                                                                                                      VARCHAR2(10)
 JOB                                                                                                                        VARCHAR2(9)
 MGR                                                                                                                        NUMBER(4)
 HIREDATE                                                                                                                   DATE
 SAL                                                                                                                        NUMBER(7,2)
 COMM                                                                                                                       NUMBER(7,2)
 DEPTNO                                                                                                                     NUMBER(2)

SQL> select * from emp10;

未选定行

SQL> --一次性将emp中所有10号部门的员工插入到emp10中
SQL> insert into emp10 select * from emp where deptno=10;

已创建 3 行。

SQL> select * from emp10;

     EMPNO ENAME      JOB              MGR HIREDATE              SAL       COMM     DEPTNO                                                                                                              
---------- ---------- --------- ---------- -------------- ---------- ---------- ----------                                                                                                              
      7782 CLARK      MANAGER         7839 09-6月 -81           2450                    10                                                                                                              
      7839 KING       PRESIDENT            17-11月-81           5000                    10                                                                                                              
      7934 MILLER     CLERK           7782 23-1月 -82           1300                    10                                                                                                              

SQL> /*
SQL> 海量插入数据
SQL> 1. 数据泵（PLSQL程序：dbms_datapump）
SQL> 2. SQL*Loader工具
SQL> 3. 外部表
SQL> */
SQL> host cls

SQL> /*
SQL> delete和truncate的区别：
SQL> 1. delete逐条删除；truncate先摧毁表 再重建
SQL> 2. (*)delete是DML（可以回滚） truncate是DDL（不可以回滚）
SQL> 3. delete不会释放空间 truncate会
SQL> 4. delete可以闪回（flashback）  truncate不可以
SQL> 5. delete会产生碎片 truncate不会
SQL> */
SQL> set feedback off
SQL> @d:\temp\testdelete.sql
SQL> select count(*) from testdelete;

  COUNT(*)                                                                                                                                                                                              
----------                                                                                                                                                                                              
      5000                                                                                                                                                                                              
SQL> set timing on
SQL> delete from testdelete;
已用时间:  00: 00: 00.10
SQL> set timing off
SQL> drop table testdelete purge;
SQL> @d:\temp\testdelete.sql
SQL> select count(*) from testdelete;

  COUNT(*)                                                                                                                                                                                              
----------                                                                                                                                                                                              
      5000                                                                                                                                                                                              
SQL> set timing on
SQL> truncate table testdelete;
已用时间:  00: 00: 02.46
SQL> set timing off
SQL> set feedback on
SQL> --原因：undo数据（还原数据）
SQL> host cls

SQL> /*
SQL> Oracle中事务的标志
SQL> 1. 起始标志:事务中第一条DML语句
SQL> 2. 结束标志：提交 显式 commit
SQL>                   隐式 正常退出exit，DDL，DCL
SQL>              回滚 显式 rollback
SQL>                   隐式 非正常退出，掉电，宕机
SQL> */
SQL> create table testsavepoint
  2  (tid number,tname varchar2(20));

表已创建。

SQL> insert into testsavepoint values(1,'Tom');

已创建 1 行。

SQL> insert into testsavepoint values(2,'Mary');

已创建 1 行。

SQL> savepoint a;

保存点已创建。

SQL> select * from testsavepoint;

       TID TNAME                                                                                                                                                                                        
---------- --------------------                                                                                                                                                                         
         1 Tom                                                                                                                                                                                          
         2 Mary                                                                                                                                                                                         

已选择 2 行。

SQL> insert into testsavepoint values(3,'Make');

已创建 1 行。

SQL> select * from testsavepoint;

       TID TNAME                                                                                                                                                                                        
---------- --------------------                                                                                                                                                                         
         1 Tom                                                                                                                                                                                          
         2 Mary                                                                                                                                                                                         
         3 Make                                                                                                                                                                                         

已选择 3 行。

SQL> rollback to savepoint a;

回退已完成。

SQL> select * from testsavepoint;

       TID TNAME                                                                                                                                                                                        
---------- --------------------                                                                                                                                                                         
         1 Tom                                                                                                                                                                                          
         2 Mary                                                                                                                                                                                         

已选择 2 行。

SQL> commit;

提交完成。

SQL> set transaction read only;

事务处理集。

SQL> select * from testsavepoint;

       TID TNAME                                                                                                                                                                                        
---------- --------------------                                                                                                                                                                         
         1 Tom                                                                                                                                                                                          
         2 Mary                                                                                                                                                                                         

已选择 2 行。

SQL> insert into testsavepoint values(3,'Make');
insert into testsavepoint values(3,'Make')
            *
第 1 行出现错误: 
ORA-01456: 不能在 READ ONLY 事务处理中执行插入/删除/更新操作 


SQL> commit;

提交完成。

SQL> spool off
