package 算法;

public class Sushu {

    /*素数只能被1和他本身整除*/
    public static void main(String[] args) {

        int j;
        for (int i=2;i<=100;i++){
            j=2;
            while (i%j!=0){
                j++;// 测试2至i的数字是否能被i整除，如不能就自加
            }
            if (j==i){// 当有被整除的数字时，判断它是不是自身
                System.out.println(i);
            }
        }

    }
}
