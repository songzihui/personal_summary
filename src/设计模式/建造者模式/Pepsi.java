package 设计模式.建造者模式;

/**
 * TODO
 *
 * @author songzihui
 * @date 2020/8/30 17:26
 */
public class Pepsi  extends ColdDrink{
    @Override
    public float price() {
        return 35.0f;
    }

    @Override
    public String name() {
        return "Pepsi";
    }
}
