package 设计模式.建造者模式;

/**
 * TODO 创建实现 Packing 接口的实体类。
 *
 * @author songzihui
 * @date 2020/8/30 17:17
 */
public class Wrapper  implements Packing {
    @Override
    public String pack() {
        return "Wrapper";
    }
}
