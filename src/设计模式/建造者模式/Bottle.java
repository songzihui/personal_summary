package 设计模式.建造者模式;

/**
 * TODO
 *
 * @author songzihui
 * @date 2020/8/30 17:20
 */
public class Bottle implements Packing {

    @Override
    public String pack() {
        return "Bottle";
    }
}
