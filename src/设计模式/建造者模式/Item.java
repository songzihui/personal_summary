package 设计模式.建造者模式;


/**
 * TODO:创建一个表示食物条目和食物包装的接口。
 * @author songzihui
 * @date 2020/8/30
 *
 */
public interface Item {

    public String name();
    public Packing packing();
    public float price();
}
