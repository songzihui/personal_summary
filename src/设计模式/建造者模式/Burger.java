package 设计模式.建造者模式;

/**
 * TODO 创建实现 Item 接口的抽象类，该类提供了默认的功能。
 *
 * @author songzihui
 * @date 2020/8/30 17:21
 */
public abstract class Burger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();
}
