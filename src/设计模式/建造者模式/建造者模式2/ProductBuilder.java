package 设计模式.建造者模式.建造者模式2;

public interface ProductBuilder {
    void builderProductName(String productName);
    void builderCompanyName(String companyName);
    void builderPart1(String part1);
    void builderPart2(String part2);
    void builderPart3(String part3);
    void builderPart4(String part4);

    Product build();
}
