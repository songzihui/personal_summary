package 设计模式.建造者模式.建造者模式2;

/**
 * TODO 将一个复杂对象的创建与他的表示分离，使得同样的构建过程可以创建 不同的表示
 *
 * @author songzihui
 * @date 2020/10/9 22:27
 */
public class BuilderTest {

    public static void main(String[] args) {
        ProductBuilder specialConcreteProductBuilder = new SpecialConcreteProductBuilder();
        Director director = new Director(specialConcreteProductBuilder);

        Product product = director.makeProduct("productNamexxx", "cn...", "part1", "part2", "part3", "part4");
        System.out.println(product);
    }
}
