package 设计模式.建造者模式;

/**
 * TODO
 *
 * @author songzihui
 * @date 2020/8/30 17:23
 */
public class VegBurger extends Burger {

    @Override
    public float price() {
        return 25.0f;
    }

    @Override
    public String name() {
        return "Veg Burger";
    }
}
