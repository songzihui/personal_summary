package 设计模式.单例模式;

/**
 * TODO 懒汉式，线程不安全
 *
 * @author songzihui
 * @date 2020/8/30 17:06
 */
public class Singleton {

    private static Singleton instance;
    private Singleton (){}

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
