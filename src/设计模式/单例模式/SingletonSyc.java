package 设计模式.单例模式;

/**
 * TODO 懒汉式，线程安全  延迟加载
 *
 * @author songzihui
 * @date 2020/8/30 17:07
 */
public class SingletonSyc {
    private static SingletonSyc instance;

    private SingletonSyc() {
    }

    public static synchronized SingletonSyc getSingletonSyc(){
        if(instance == null){
            instance = new SingletonSyc();
        }
        return instance;
    }
}

