package 设计模式.单例模式;

/**
 * TODO  1、饿汉式，  线程安全
 *
 * @author songzihui
 * @date 2020/8/30 17:04
 */
public class SingleObject {
    //创建 SingleObject 的一个对象
    private static SingleObject instance = new SingleObject();

    //让构造函数为 private，这样该类就不会被实例化
    private SingleObject(){}

    //获取唯一可用的对象
    public static SingleObject getInstance(){
        return instance;
    }

    public void showMessage(){
        System.out.println("Hello World!");
    }

}
