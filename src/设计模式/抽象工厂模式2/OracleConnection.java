package 设计模式.抽象工厂模式2;

/**
 * TODO
 *
 * @author songzihui
 * @date 2020/10/9 21:55
 */
public class OracleConnection implements IConnection {
    @Override
    public void connect() {
        System.out.println("oracle connected.");
    }
}
