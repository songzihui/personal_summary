package 设计模式.抽象工厂模式2;

/**
 * TODO
 *
 * @author songzihui
 * @date 2020/10/9 21:57
 */
public class MysqlDataBaseUtils implements IDatabaseUtils {
    @Override
    public IConnection getConnection() {
        return new MysqlConnection();
    }

    @Override
    public ICommand getCommand() {
        return new MysqlCommand();
    }
}
