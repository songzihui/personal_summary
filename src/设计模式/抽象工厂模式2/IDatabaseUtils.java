package 设计模式.抽象工厂模式2;

public interface IDatabaseUtils {
    IConnection getConnection();
    ICommand getCommand();
}
