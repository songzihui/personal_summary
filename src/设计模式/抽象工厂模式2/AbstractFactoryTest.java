package 设计模式.抽象工厂模式2;

/**
 * TODO
 *
 * @author songzihui
 * @date 2020/10/9 22:04
 */
public class AbstractFactoryTest {

    public static void main(String[] args) {
        IDatabaseUtils iDatabaseUtils=new OracleDataBaseUtils();
        ICommand command = iDatabaseUtils.getCommand();
        IConnection connection = iDatabaseUtils.getConnection();
        command.command();
        connection.connect();


        IDatabaseUtils databaseUtils=new MysqlDataBaseUtils();

    }
}
