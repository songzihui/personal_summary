package 设计模式.抽象工厂模式2;

/**
 * TODO
 *
 * @author songzihui
 * @date 2020/10/9 22:00
 */
public class OracleDataBaseUtils implements  IDatabaseUtils {
    @Override
    public IConnection getConnection() {
        return new OracleConnection();
    }

    @Override
    public ICommand getCommand() {
        return new OracleCommand();
    }
}
