package 设计模式.抽象工厂模式;

public interface Shape {
    void draw();



    //创建实现接口的实体类。
    public class Rectangle implements Shape {

        @Override
        public void draw() {
            System.out.println("矩形::draw() method.");
        }
    }

    public class Square implements Shape {

        @Override
        public void draw() {
            System.out.println("正方形::draw() method.");
        }
    }

    public class Circle implements Shape {

        @Override
        public void draw() {
            System.out.println("Inside Circle::draw() method.");
        }
    }
}
