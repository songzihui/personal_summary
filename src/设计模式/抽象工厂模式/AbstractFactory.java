package 设计模式.抽象工厂模式;

/**
 * TODO:为 Color 和 Shape 对象创建抽象类来获取工厂。
 * @author songzihui
 * @date 2020/8/30
 *
 */
public abstract class AbstractFactory {

    public abstract Color getColor(String color);
    public abstract Shape getShape(String shape) ;
}
