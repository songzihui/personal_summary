package 设计模式.抽象工厂模式;

/**
 * TODO 创建扩展了 AbstractFactory 的工厂类，基于给定的信息生成实体类的对象。
 * @author songzihui
 * @date 2020/8/30 16:49
 */
public  class ShapeFactory  extends AbstractFactory{
    @Override
    public Color getColor(String color) {
        return null;
    }

    @Override
    public Shape getShape(String shapeType) {
        if(shapeType == null){
            return null;
        }
        if(shapeType.equalsIgnoreCase("CIRCLE")){
            return new Shape.Circle();
        } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
            return new Shape.Rectangle();
        } else if(shapeType.equalsIgnoreCase("SQUARE")){
            return new Shape.Square();
        }
        return null;
    }
}
