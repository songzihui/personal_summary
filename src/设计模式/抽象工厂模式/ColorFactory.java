package 设计模式.抽象工厂模式;

/**
 * TODO 创建扩展了 AbstractFactory 的工厂类，基于给定的信息生成实体类的对象
 *
 * @author songzihui
 * @date 2020/8/30 16:54
 */
public class ColorFactory  extends AbstractFactory{
    @Override
    public Color getColor(String color) {
        if(color == null){
            return null;
        }
        if(color.equalsIgnoreCase("RED")){
            return new Color.Red();
        } else if(color.equalsIgnoreCase("GREEN")){
            return new Color.Green();
        } else if(color.equalsIgnoreCase("BLUE")){
            return new Color.Blue();
        }
        return null;
    }

    @Override
    public Shape getShape(String shape) {
        return null;
    }
}
