package 设计模式.工厂模式;

/**
 * TODO：形状
 *
 * @author songzihui
 * @date 2020/8/30 15:54
 */
public interface Shape {

    void draw();



    //创建实现接口的实体类。正方形
    public class Square implements Shape {

        @Override
        public void draw() {
            System.out.println("这是画正方形的方法::draw() method.");
        }
    }
    //创建实现接口的实体类。圆形
    public class Circle implements Shape {

        @Override
        public void draw() {
            System.out.println("这是画园形的方法::draw() method.");
        }
    }
    public class Rectangle implements Shape {

        @Override
        public void draw() {
            System.out.println("Inside Rectangle::draw() method.");
        }
    }

}


