package 设计模式.工厂模式;

/**
 * TODO 创建一个工厂，生成基于给定信息的实体类的对象。
 *
 * @author songzihui
 * @date 2020/8/30 15:57
 */
public class ShapeFactory {

    //使用 getShape 方法获取形状类型的对象
    public Shape getShape(String shapeType){
        if(shapeType == null){
            return null;
        }
        if(shapeType.equalsIgnoreCase("圆形")){
            return new Shape.Circle();
        } else if(shapeType.equalsIgnoreCase("矩形")){
            return new Shape.Rectangle();
        } else if(shapeType.equalsIgnoreCase("正方形")){
            return new Shape.Square();
        }
        return null;
    }
}
