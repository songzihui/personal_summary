package java的单例模式;

public class Singleton2 {

    public static void main(String[] args) {
        //这种调用出错 ，构造函数私有化，所以只有通过下面方法的形式去调用
//        Singleton singleton2=new Singleton();
        //在外部使用调用单例模式的方法
        Singleton singleton=Singleton.getInstance();
    }
}
