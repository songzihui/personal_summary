package java的单例模式;

//懒汉，线程不安全
public class Singleton {

    //对象类型的变量
    private static Singleton instance;

    //构造方法私有后-----我们不允许外部类，其他类去new创建这个对象,但是私有化后自己可以调用
    private Singleton() {
    }

    //只有通过这个方法去创建类--对象类型的变量
    public static  Singleton getInstance(){
        if (instance==null){
            instance=new Singleton();
        }
        return instance;
    }
    Singleton singleton=new Singleton();
}
