package java的单例模式;

//懒汉，线程安全----这种写法能够在多线程中很好的工作，而且看起来它也具备很好的lazy loading，但是，遗憾的是，效率很低，99%情况下不需要同步。
public class Singleton3 {
    private static Singleton3 singleton3;

    private Singleton3() {
    }

    public static synchronized Singleton3 getSingleton3(){
        if (singleton3==null){
            singleton3=new Singleton3();
        }
        return singleton3;
    }
}
