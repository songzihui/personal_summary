package 链表;

public class SingleLinkedList {
    final static String str="hello world"; // 实例变量
    private int size;//链表节点的个数
    private Node head;//头节点
    /*多说一句，头节点的成员变量的定义，采用了内部类的定义方式，java变量以数据类型划分，可以是基本数据类型和引用数据类型
    * 引用数据类型包括类、接口、数组，private Node head，node为内部类的内名称的类型
    * */

    public SingleLinkedList() {
        size = 0;
        head = null;
    }

    //链表的每个节点类
    private class Node {
        private Object data;//每个节点的数据
        private Node next;//每个节点指向下一个节点的连接

        public Node(Object data) {
            this.data = data;
        }
    }

    //在链表头添加元素
    public Object addHead(Object obj) {
        Node node = new Node(obj);
        if (size == 0) {
            head = node;//如果没有元素，对象就是头，node指向head
        } else {
            node.next = head;//(head就是node对象，把head这个头节点的node对象，放到node的next里面，然后next里面存放下一个的data和next)
            // 先把当前的head节点的值放给node.next(当前节点next属性放下一个值)下一个值
            //每次保存的时候都是从head头结点插入，把头结点这个对象放到node对象的next里面，然后把当前node对象，定义为head对象，head对象里面的值就是data，next就是空
            head = node;//在表头增加一个节点node，把其设置为head
            //node节点里面有data，和next，每次添加数据next后面加上下一个data，和next
        }
        size++;
        return obj;
    }

    //在链表头删除元素
    public Object deleteHead() {
        Object obj =head.data;
        head = head.next;
        size--;
        return obj;
    }

    //查找指定元素，找到了返回节点Node，找不到返回null
    public Node find(Object obj) {
        Node current = head;
        int tempSize = size;
        while (tempSize > 0) {
            if (obj.equals(current.data)) {//查找obj这个节点的值和从头节点遍历往后的data是否相等，返回当前遍历的current
                return current;
            } else {
                current = current.next;//否则node节点里面next取出来再去遍历
            }
            tempSize--;
        }
        return null;
    }

    //删除指定的元素，删除成功返回true
    public boolean delete(Object value) {
        if (size == 0) {
            return false;
        }
        Node current = head;
        Node previous = head;
        while (current.data != value) {//从头节点开始遍历，看是否等于要删除的值
            if (current.next == null) {//如果当前节点的next是空，则说明没有值，不能删除
                return false;
            } else {
                previous = current;//如果等于删除的值，把current这个node节点赋给previous
                current = current.next;//把current这个节点的next,下一个节点node赋给current
            }
        }
        //如果删除的节点是第一个节点
        if (current == head) {
            head = current.next;
            size--;
        } else {//删除的节点不是第一个节点
            previous.next = current.next;//把删除的下一个node给到删除当前的上一个node的next，连接成功
            size--;
        }
        return true;
    }

    //判断链表是否为空
    public boolean isEmpty() {
        return (size == 0);
    }

    //显示节点信息
    public void display() {
        if (size > 0) {
            Node node = head;
            int tempSize = size;
            if (tempSize == 1) {//当前链表只有一个节点
                System.out.println("[" + node.data + "]");
                return;
            }
            while (tempSize > 0) {
                if (node.equals(head)) { //如果node节点等于头结点，打印头结点
                    System.out.print("[" + node.data + "->");
                } else if (node.next == null) {//如果节点是最后一个节点，node.next后面跟的node是空打印最后一个
                    System.out.print(node.data + "]");
                } else {//否则都是中间的节点，打印中间的
                    System.out.print(node.data + "->");
                }
                node = node.next;//把node对象的next里面的node
                tempSize--;//长度减一
            }
            System.out.println();
        } else {//如果链表一个节点都没有，直接打印[]
            System.out.println("[]");
        }

    }

    public static void main(String[] args) {

        SingleLinkedList singleList = new SingleLinkedList();
        singleList.addHead("A");
        singleList.addHead("B");
        singleList.addHead("C");
        singleList.addHead("D");
        //打印当前链表信息
        singleList.display();
        //删除C
        singleList.delete("C");
        singleList.display();
        //查找B
        System.out.println(singleList.find("B"));

    }
    //https://www.cnblogs.com/ysocean/p/7928988.html
}


