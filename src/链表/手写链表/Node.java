package 链表.手写链表;

/**
 * TODO 单链表
 *
 * @author songzihui
 * @date 2020/8/30 23:10
 */
public class Node {
    public Object data;
    public Node next;
    private Node head;

    public Node(Object data) {
        this.data = data;
    }

    /**
     * TODO:
     *
     * @author songzihui
     * @date 2020/8/30
     */
    public void addNode(Node node) {
        //链表中有节点、遍历到最后一个结点
        Node temp = head; //头结点
        while (temp.next != null) {
            temp = temp.next;//当前节点-----存下一个节点的地址
        }
        temp.next = node;
    }
}
