package 链表;

import java.util.LinkedList;

public class LinkedList源码分析 {

    /**
     * 1. 概览
     *
     * 基于双向链表实现，使用 Node 存储链表节点信息。
     *
     * 每个链表存储了 first 和 last 指针：
     */

    /*private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;
    }*/

    /*transient Node<E> first;
    transient Node<E> last;*/

    /**
     *
     * 2. 与 ArrayList 的比较
     *
     * ArrayList 基于动态数组实现，LinkedList 基于双向链表实现；
     * ArrayList 支持随机访问，LinkedList 不支持；因为ArrayList是基于数组实现的，可以通过下标去查找，而链表查找数据不具备随机访问，必须通过头去遍历查找
     * LinkedList 在任意位置添加删除元素更快。因为LinkedList弱化了位置的概念，他只找到前面的节点，添加和删除进去就可以了，跟其他位置上他没有关系。
     */
}
