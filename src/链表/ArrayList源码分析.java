package 链表;

import java.util.ArrayList;

public class ArrayList源码分析 {

    /**
     *1. 概览
     * 实现了 RandomAccess 接口，因此支持随机访问。这是理所当然的，因为 ArrayList 是基于数组实现的。
     *
     * public class ArrayList<E> extends AbstractList<E>
     *         implements List<E>, RandomAccess, Cloneable, java.io.Serializable
     *
     * 数组的默认大小是10
     * private static final int DEFAULT_CAPACITY = 10;
     *
     * 2. 扩容
     * 添加元素时使用 ensureCapacityInternal() 方法来保证容量足够，如果不够时，需要使用 grow() 方法进行扩容，
     * 新容量的大小为 oldCapacity + (oldCapacity >> 1)，也就是旧容量的 1.5 倍。 >>位运算符缩小2倍 <<扩大2的倍数
     *
     *
     */
  /*  public boolean add(E e) {
        ensureCapacityInternal(size + 1);  // Increments modCount!!
        elementData[size++] = e;
        return true;
    }

    private void ensureCapacityInternal(int minCapacity) {
        if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
            minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        ensureExplicitCapacity(minCapacity);
    }

    private void ensureExplicitCapacity(int minCapacity) {
        modCount++;
        // overflow-conscious code
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }

    private void grow(int minCapacity) {
        // overflow-conscious code
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            newCapacity = hugeCapacity(minCapacity);
        // minCapacity is usually close to size, so this is a win:
        elementData = Arrays.copyOf(elementData, newCapacity);
    }*/

    /**
     * 3. 删除元素
     *
     * 需要调用 System.arraycopy() 将 index+1 后面的元素都复制到 index 位置上，
     *
     * 该操作的时间复杂度为 O(N)，可以看出 ArrayList 删除元素的代价是非常高的。
     *
     */

    /*public E remove(int index) {
        rangeCheck(index);
        modCount++;
        E oldValue = elementData(index);
        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(elementData, index+1, elementData, index, numMoved);
        elementData[--size] = null; // clear to let GC do its work
        return oldValue;
    }*/



    /**
     *
     * Vector
     *
     * 1. 同步
     *
     * 它的实现与 ArrayList 类似，但是使用了 synchronized 进行同步。
     */

    /*public synchronized boolean add(E e) {
        modCount++;
        ensureCapacityHelper(elementCount + 1);
        elementData[elementCount++] = e;
        return true;
    }

    public synchronized E get(int index) {
        if (index >= elementCount)
            throw new ArrayIndexOutOfBoundsException(index);

        return elementData(index);
    }*/


    /**
     * 2. 与 ArrayList 的比较
     *
     * Vector 是同步的，因此开销就比 ArrayList 要大，访问速度更慢。最好使用 ArrayList 而不是 Vector，因为同步操作完全可以由程序员自己来控制；
     * Vector 每次扩容请求其大小的 2 倍空间，而 ArrayList 是 1.5 倍。
     */

    /**
     * 3. 替代方案
     *
     * 可以使用 Collections.synchronizedList(); 得到一个线程安全的 ArrayList。
     *
     * List<String> list = new ArrayList<>();
     * List<String> synList = Collections.synchronizedList(list);
     *
     * 也可以使用 concurrent 并发包下的 CopyOnWriteArrayList 类。
     *
     * List<String> list = new CopyOnWriteArrayList<>();
     */
}
