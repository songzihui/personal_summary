package 排序;

import java.util.Arrays;
//归并排序2
public class MergeSort2 {

    public static void main(String[] args) {
        int [] arr=new int[]{2,4,6,1,65,4,3,7,0,78,5};
        //输出原数组
        System.out.println(Arrays.toString(arr));
        //输出归并后的数组 长度下标的位置
        mergeSort(arr,0,arr.length-1);
        System.out.println(Arrays.toString(arr));
    }
    //归并排序
    public static  void mergeSort(int arr[],int low,int high){
        int middle=(low+high)/2;
        if (low<high){
            //归并左边
            mergeSort(arr,low,middle);
            //归并右边(middle+1为右边第一个指针)
            mergeSort(arr,middle+1,high);
            //归并
            merge(arr,low,middle,high);
        }
    }

    public static void merge(int arr[],int low,int middle,int high){
        //用于储存归并后的临时数组
        int[] temp = new int[high-low+1]; //或者是int []temp=new int[high-low+1];
        //记录第一个数组中需要遍历的下标
        int i=low;
        //记录第二个数组中需要遍历的下标
        int j=middle+1;
        //用于记录在临时数组中存放的下标
        int index=0;
        //遍历两个数组，取出最小的数组，放在临时数组中
        while (i<=middle &&j<=high){
            //如果前面的数小于后面的数
            if (arr[i]<arr[j]){
                temp[index]=arr[i];
                i++;
            }else {
                temp[index]=arr[j];
                j++;
            }
            index++;
        }
        while (i<=middle){
            temp[index]=arr[i];
            i++;
            index++;
        }
        while (j<=high){
            temp[index]=arr[j];
            j++;
            index++;
        }
        //把临时数组中的数据重新存入远数组中
        for (int k = 0; k <temp.length ; k++) {
            arr[k+low]=temp[k];
        }
    }
}
