package 排序;
//https://blog.csdn.net/yushiyi6453/article/details/76407640
/*
* 直接插入排序插入排序从小到大排序：首先位置1上的数和位置0上的数进行比较，如果位置1上的数大于位置0上的数，将位置0上的数向后移一位，将1插入到0位置，否则不处理。位置k上的数和之前的数依次进行比较，如果位置K上的数更大，将之前的数向后移位，最后将位置k上的数插入不满足条件点，反之不处理。
* */
public class InsertSort {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        int a[] = {3, 9, 12,7, 2, 4, 9, 6};
        //a的长度是8，所存放的地址下标为[0-7]
        new InsertSort().insertSort(a);
    }
    private void insertSort(int[] a) {
        System.out.println("———————————————————直接插入排序算法—————————————————————");
        //如果后面突然来了一个最小的数字，需要移动把之前的数组往后移动，效率低
        int n = a.length;
        int i, j;
        for (i = 1; i < n; i++) {
            //temp为本次循环待插入有序列表中的数
            int temp = a[i];//下标为1的元素数，下标1以前的元素做比较如果1以前的元素大就交换，把后面所有的元素向后移
            // 寻找temp插入有序列表的正确位置
            for (j = i - 1; j >= 0 && a[j] > temp; j--) {
                /*i=1; j=1-1=0;  arr[0] arr[1]*/
                /*i=2; j=2-1=1;  arr[1]..[0] arr[2] */
                //元素后移，为插入temp做准备
                a[j + 1] = a[j];
            }
            //插入temp
            a[j + 1] = temp;
            print(a, n, i);
        }
        printResult(a, n);
    }
    /**
     * 打印排序的最终结果
     *
     * @param a
     * @param n
     */
    private void printResult(int[] a, int n) {
        System.out.print("最终排序结果：");
        for (int j = 0; j < n; j++) {
            System.out.print(" " + a[j]);
        }
        System.out.println();
    }

    /**
     * 打印排序的每次循环的结果
     *
     * @param a
     * @param n
     * @param i
     */
    private void print(int[] a, int n, int i) {
        // TODO Auto-generated method stub
        System.out.print("第" + i + "次：");
        for (int j = 0; j < n; j++) {
            System.out.print(" " + a[j]);
        }
        System.out.println();
    }
}
