package 栈和队列;

public class Stack {

    /**
     * 概念很简单，栈 (Stack)是一种后进先出(last in first off，LIFO)的数据结构，而队列(Queue)则是一种先进先出 (fisrt in first out，FIFO)的结构
     *  栈只能在顶部进出，最后进来的最先出去 后进先出（刚刚操作的在还原回去）
     *  要实现这些功能，我们有两中方法，数组和链表，先看链表实现： 数组也可以实现栈，但是数组每次扩容需要成倍扩容，修改位置需要整体index-1或者加1效率低
     *
     *  队列：Queue是一种先进先出的数据结构，和Stack一样，他也有链表和数组两种实现，理解了Stack的实现后，Queue的实现就比较简单了。
     *  queue有前后两个口，一个口进，一个口出，先进来的先出去
     *
     */

    /**
     * Stack和Queue的应用
     * Stack这种数据结构用途很广泛，比如编译器中的词法分析器、Java虚拟机、软件中的撤销操作、浏览器中的回退操作，编译器中的函数调用实现等等。
     *
     *Queue的应用
     * 在现实生活中Queue的应用也很广泛，最广泛的就是排队了，”先来后到” First come first service ，以及Queue这个单词就有排队的意思。
     * 还有，比如我们的播放器上的播放列表，我们的数据流对象，异步的数据传输结构(文件IO，管道通讯，套接字等)
     * 还有一些解决对共享资源的冲突访问，比如打印机的打印队列等。消息队列等。交通状况模拟，呼叫中心用户等待的时间的模拟等等。
     *
     */

/**
 * 基于链表的stack
 */
   /* class Node
    {
        public T Item{get;set;}
        public Node Next { get; set; }
    }
    private Node first = null;
    private int number = 0;*/


    /*push添加方法*/

  /*  void Push(T node)
    {
        Node oldFirst = first;
        first = new Node();
        first.Item= node;
        first.Next = oldFirst;
        number++;
    }*/

    /*Pop方法移除*/

 /*   T Pop()
    {
        T item = first.Item;
        first = first.Next;
        number--;
        return item;
    }*/


    /**
     * 基于链表的dequeue
     */


    /*public T Dequeue()
    {
        T temp = first.Item;
        first = first.Next;
        number--;
        if (IsEmpety())
            last = null;
        return temp;
    }*/


}
