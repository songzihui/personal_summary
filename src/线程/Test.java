package 线程;

//设计四个线程，其中两个线程执行加操作，两个线程执行减操作
public class Test implements Runnable{

    private static int count=0;

    @Override
    public void run() {
        while (true){
                if (Thread.currentThread().getName().startsWith("add")){
                    count++;
                }else {
                    count--;
                }
            System.out.println(count);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
        }
    }

    public static void main(String[] args) {
        Test t=new Test();
        Thread t1=new Thread(t,"add1");
        Thread t2=new Thread(t,"add2");
        Thread t3=new Thread(t,"mul03");
        Thread t4=new Thread(t,"mul04");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
